DROP TABLE IF EXISTS `fs2`.`vpodobalky`;
DROP TABLE IF EXISTS `fs2`.`gralky`;
DROP TABLE IF EXISTS `fs2`.`zbirky`;
DROP TABLE IF EXISTS `fs2`.`sluhachy`;

CREATE TABLE IF NOT EXISTS `fs2`.`sluhachy` (
  `id` BIGINT NOT NULL UNIQUE AUTO_INCREMENT,
  `email` VARCHAR(255) NOT NULL UNIQUE,
  `password` VARCHAR(255) NOT NULL,
  `role` ENUM ('ADMIN', 'USER'),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `fs2`.`zbirky` (
  `id` BIGINT NOT NULL UNIQUE AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `fs2`.`gralky` (
  `id` BIGINT NOT NULL UNIQUE AUTO_INCREMENT,
  `fk_zbirka_id` BIGINT NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  CONSTRAINT UNIQUE (`fk_zbirka_id`, `title`),
  PRIMARY KEY (`id`),
  FOREIGN KEY (`fk_zbirka_id`) REFERENCES `fs2`.`zbirky`(`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `fs2`.`vpodobalky` (
  `fk_sluhachy_id` BIGINT NOT NULL,
  `fk_gralky_id` BIGINT NOT NULL,
  PRIMARY KEY (`fk_sluhachy_id`, `fk_gralky_id`),
  FOREIGN KEY (`fk_sluhachy_id`) REFERENCES `fs2`.`sluhachy`(`id`),
  FOREIGN KEY (`fk_gralky_id`) REFERENCES `fs2`.`gralky`(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


