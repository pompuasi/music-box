package ua.danit.musicbox.configuration.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.danit.musicbox.dao.UserRepo;
import ua.danit.musicbox.model.entity.User;

@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

  private static final Logger LOGGER = LoggerFactory.getLogger(CustomUserDetailsService.class);

  private final UserRepo userRepo;

  @Autowired
  public CustomUserDetailsService(UserRepo userRepo) {
    this.userRepo = userRepo;
  }

  @Transactional(readOnly=true)
  public UserDetails loadUserByUsername(String email)
      throws UsernameNotFoundException {
    User user = userRepo.findByEmail(email);
    LOGGER.info("User : {}", user);
    if(user==null){
      LOGGER.info("User not found");
      throw new UsernameNotFoundException("Username not found");
    }
    LOGGER.info("User " + user.getEmail() + " success.");
//    return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), user.isEnabled(), true, true, user.isAccountNonLocked(), user.getAuthorities());
    return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), user.getAuthorities());
  }

}