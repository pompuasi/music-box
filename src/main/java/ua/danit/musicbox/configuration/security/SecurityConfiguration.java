package ua.danit.musicbox.configuration.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

  @Autowired
  private AuthenticationEntryPoint entryPoint;

  @Autowired
  private @Qualifier("customUserDetailsService") UserDetailsService userDetailsService;

  @Autowired
  private SimpleUrlAuthenticationSuccessHandler successHandler;

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
        .csrf().disable()
        .httpBasic()
        .authenticationEntryPoint(entryPoint)
        .and()
        .authorizeRequests()
        .antMatchers("/**").permitAll()
//        .antMatchers("/api/user/register").permitAll()
//        .antMatchers("/api/**").access("hasRole('ROLE_ADMIN')")
////        .antMatchers("/music-box").access("hasRole('ROLE_USER')")
//        .antMatchers("/music-box").authenticated()
        .and()
        .formLogin().loginPage("/").permitAll()
        .loginProcessingUrl("/login")
        .usernameParameter("email").passwordParameter("password")
        .successHandler(successHandler)
        .failureHandler(new SimpleUrlAuthenticationFailureHandler())
        .and()
        .exceptionHandling()
        .and()
        .logout();
  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Bean
  public DaoAuthenticationProvider authenticationProvider() {
    DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
    authenticationProvider.setUserDetailsService(userDetailsService);
    authenticationProvider.setPasswordEncoder(passwordEncoder());
    return authenticationProvider;
  }

}
