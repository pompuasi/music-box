package ua.danit.musicbox.configuration.security;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Set;

@Component
public class AuthSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

  @Getter
  @Setter
  private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

  private RequestCache requestCache = new HttpSessionRequestCache();

  @Override
  public void onAuthenticationSuccess(
      HttpServletRequest request,
      HttpServletResponse response,
      Authentication authentication) throws IOException, ServletException {

    SavedRequest savedRequest = requestCache.getRequest(request, response);

    if (savedRequest == null) {
      clearAuthenticationAttributes(request);
      return;
    }

    String targetUrlParam = getTargetUrlParameter();

    if (isAlwaysUseDefaultTargetUrl()
        || (targetUrlParam != null
        && StringUtils.hasText(request.getParameter(targetUrlParam)))) {
      requestCache.removeRequest(request, response);
      clearAuthenticationAttributes(request);
      return;
    }

    redirectStrategy.sendRedirect(request, response, "/api/user/");
    clearAuthenticationAttributes(request);
  }

  public void setRequestCache(RequestCache requestCache) {
    this.requestCache = requestCache;
  }

//  @Override
//  public void onAuthenticationSuccess(HttpServletRequest request,
//                                      HttpServletResponse response, Authentication authentication) throws IOException,
//      ServletException {
//    HttpSession session = request.getSession();
//
//    /*Set some session variables*/
//    User authUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//    session.setAttribute("email", authUser.getUsername());
//    session.setAttribute("authorities", authentication.getAuthorities());
//
//    /*Set target URL to redirect*/
//    String targetUrl = determineTargetUrl(authentication);
//    Object s = response.toString();
//    redirectStrategy.sendRedirect(request, response, targetUrl);
//  }

  protected String determineTargetUrl(Authentication authentication) {
    Set<String> authorities = AuthorityUtils.authorityListToSet(authentication.getAuthorities());
    if (authorities.contains("ADMIN")) {
      return "/api/user/";
    } else if (authorities.contains("USER")) {
      return "/";
    } else {
      throw new IllegalStateException();
    }
  }
}
