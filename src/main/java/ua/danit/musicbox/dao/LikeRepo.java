package ua.danit.musicbox.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.danit.musicbox.model.entity.Like;

import java.util.List;

@Repository
public interface LikeRepo extends JpaRepository<Like, Long> {
    List<Like> findAllByLikePK_User_Id(Long id);

    List<Like> findAllByLikePK_Track_Id(Long id);

    List<Like> deleteAllByLikePK_User_Id(Long id);

}
