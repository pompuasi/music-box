package ua.danit.musicbox.services;

import ua.danit.musicbox.model.entity.Like;

import java.util.List;

public interface LikeService {
    List<Like> findAll();
    List<Like> findByUserId(Long user_id);
    Like addLikeToTrack(Like like);
    Like deleteLike(Long id);
}
