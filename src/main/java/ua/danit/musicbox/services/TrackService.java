package ua.danit.musicbox.services;

import ua.danit.musicbox.model.entity.Track;

import java.util.List;

public interface TrackService {

    List<Track> findAll();
    Track findById(Long id);
    Track add(Track track);
    Track delete(Long id);
}
