package ua.danit.musicbox.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.danit.musicbox.dao.TrackRepo;
import ua.danit.musicbox.model.entity.Track;
import ua.danit.musicbox.services.TrackService;

import java.util.List;

@Service
public class TrackServiceImpl implements TrackService {

    private final TrackRepo trackRepo;

    @Autowired
    public TrackServiceImpl(TrackRepo trackRepo) {
        this.trackRepo = trackRepo;
    }

    @Override
    public List<Track> findAll() {
        return trackRepo.findAll();
    }

    @Override
    public Track findById(Long id) {
        return trackRepo.getOne(id);
    }

    @Override
    public Track add(Track track) {
        return trackRepo.save(track);
    }

    @Override
    public Track delete(Long id) {
        trackRepo.deleteById(id);

        return null;
    }
}
