package ua.danit.musicbox.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.danit.musicbox.dao.AlbumRepo;
import ua.danit.musicbox.model.entity.Album;
import ua.danit.musicbox.services.AlbumService;

import java.util.List;

@Service
public class AlbumServiceImpl implements AlbumService {


    private final AlbumRepo albumRepo;

    @Autowired
    public AlbumServiceImpl(AlbumRepo albumRepo) {
        this.albumRepo = albumRepo;
    }

    @Override
    public List<Album> findAll() {
        return albumRepo.findAll();
    }

    @Override
    public Album findById(Long id) {
        return albumRepo.getOne(id);
    }

    @Override
    public Album add(Album album) {
        return albumRepo.save(album);
    }

    @Override
    public Album delete(Long id) {
        albumRepo.deleteById(id);
        return null;
    }
}
