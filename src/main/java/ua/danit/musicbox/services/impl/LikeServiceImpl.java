package ua.danit.musicbox.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.danit.musicbox.dao.LikeRepo;
import ua.danit.musicbox.model.entity.Like;
import ua.danit.musicbox.services.LikeService;

import java.util.List;

@Service
public class LikeServiceImpl implements LikeService {

    private final LikeRepo likeRepo;

    @Autowired
    public LikeServiceImpl(LikeRepo likeRepo) {
        this.likeRepo = likeRepo;
    }

    @Override
    public List<Like> findAll() {
        return likeRepo.findAll();
    }

    @Override
    public List<Like> findByUserId(Long user_id) {
        List<Like> likes = likeRepo.findAllByLikePK_User_Id(user_id);
        return likes;
    }

    @Override
    public Like addLikeToTrack(Like like) {
        return likeRepo.save(like);
    }

    @Override
    public Like deleteLike(Long id) {
        return (Like) likeRepo.deleteAllByLikePK_User_Id(id);
    }
}

