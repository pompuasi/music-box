package ua.danit.musicbox.services;

import ua.danit.musicbox.model.entity.Album;

import java.util.List;

public interface AlbumService {
    List<Album> findAll();
    Album findById(Long id);
    Album add(Album album);
    Album delete(Long id);

}
