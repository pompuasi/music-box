package ua.danit.musicbox.model.entity;

import java.util.ArrayList;
import java.util.List;

public class AlbumDTO {
    private Long id;
    private String title;
    private List<TrackDTO> tracks;


    public AlbumDTO() {
    }

    public AlbumDTO(Album album) {
        this.id = album.getId();
        this.title = album.getTitle();

        if (album.getTracks() != null){
            this.tracks = new ArrayList<>();
            for (Track track : album.getTracks()) {
                this.tracks.add(new TrackDTO(track));
            }
        }
    }


    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public List<TrackDTO> getTracks() {
        return tracks;
    }


    public void setId(Long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setTracks(List<TrackDTO> tracks) {
        this.tracks = tracks;
    }
}
