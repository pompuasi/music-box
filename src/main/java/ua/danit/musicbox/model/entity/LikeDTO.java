package ua.danit.musicbox.model.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LikeDTO {
  private User user;
  private TrackDTO track;

  public LikeDTO(Like like) {
    user = like.getLikePK().user;
    track = new TrackDTO(like.getLikePK().track);
  }
}
