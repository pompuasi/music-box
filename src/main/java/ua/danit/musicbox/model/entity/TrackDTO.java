package ua.danit.musicbox.model.entity;

public class TrackDTO {
    private Long id;
    private Long album_id;
    private String title;

    public Long getId() {
        return id;
    }

    public Long getAlbum_id() {
        return album_id;
    }

    public String getTitle() {
        return title;
    }

    public TrackDTO() {
    }

    public TrackDTO(Track track) {
        this.id = track.getId();
        this.title = track.getTitle();
        this.album_id = track.getAlbum().getId();
    }


    public void setId(Long id) {
        this.id = id;
    }

    public void setAlbum_id(Long album_id) {
        this.album_id = album_id;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
