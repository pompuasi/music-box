package ua.danit.musicbox.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.danit.musicbox.model.entity.Album;
import ua.danit.musicbox.model.entity.AlbumDTO;
import ua.danit.musicbox.services.AlbumService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api")

public class AlbumController {

    private final AlbumService albumService;

    @Autowired
    public AlbumController(AlbumService albumService) {
        this.albumService = albumService;
    }

    @RequestMapping(value = "/album/{id}", method = RequestMethod.GET)
    public ResponseEntity<AlbumDTO> getAlbumById(@PathVariable("id") Long id){
        AlbumDTO album = new AlbumDTO(albumService.findById(id));
        return ResponseEntity.ok().body(album);
    }

    @RequestMapping(value = "/album", method = RequestMethod.GET)
    public ResponseEntity<List<AlbumDTO>> getAllAlbums(){
        List<AlbumDTO> albumDTOS;
        List<Album> albums = albumService.findAll();
        albumDTOS = albums.stream()
                .map(AlbumDTO::new)
                .collect(Collectors.toList());

        return ResponseEntity.ok().body(albumDTOS);
    }

    @RequestMapping(value = "/album/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<AlbumDTO> deleteAlbum(@PathVariable("id") Long id) {
        AlbumDTO album = new AlbumDTO(albumService.delete(id));
        return ResponseEntity.ok().body(album);
    }

    @RequestMapping(value = "/album/", method = RequestMethod.POST)
    public ResponseEntity<Album> addAlbum(@RequestBody Album album) {
        return ResponseEntity.ok().body(albumService.add(album));
    }

}
