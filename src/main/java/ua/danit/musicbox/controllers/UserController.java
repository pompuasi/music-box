package ua.danit.musicbox.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import ua.danit.musicbox.model.entity.User;
import ua.danit.musicbox.services.UserService;
import java.util.List;


@RestController
@RequestMapping(value = "/api/user")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<User> getUserById(@PathVariable("id") Long id) {
        User user = userService.findById(id);
        return ResponseEntity.ok().body(user);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<List<User>> getAllUsers(){
        return ResponseEntity.ok().body(userService.findAll());
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public ResponseEntity<User> deleteUser(@PathVariable("id") Long id){
        User user = userService.remove(id);
        return ResponseEntity.ok().body(user);
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<?> addUser(@RequestParam("email") String email,
                                     @RequestParam("psw") String psw)  {
        User user = new User();
        user.setEmail(email);
        user.setPassword(psw);
        user.setRole(User.Role.USER);
        userService.add(user);
       return ResponseEntity.ok("user created");
    }

}
