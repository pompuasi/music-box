package ua.danit.musicbox.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ViewController {

  private final Logger LOGGER = LoggerFactory.getLogger(ViewController.class);

  @RequestMapping(value = "/")
  public String index(){
    LOGGER.info("index visited");
    return "reg";
  }

  @RequestMapping(value = "/music-box")
  public String musicBox() {
    return "index";
  }
}
