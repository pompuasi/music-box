package ua.danit.musicbox.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ua.danit.musicbox.model.entity.Like;
import ua.danit.musicbox.model.entity.LikeDTO;
import ua.danit.musicbox.services.LikeService;
import ua.danit.musicbox.services.TrackService;
import ua.danit.musicbox.services.UserService;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(value = "/api/like")
public class LikeController {

    private final LikeService likeService;
    private final UserService userService;
    private final TrackService trackService;

    @Autowired
    public LikeController(LikeService likeService, UserService userService, TrackService trackService) {
        this.likeService = likeService;
        this.userService = userService;
        this.trackService = trackService;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<List<Like>> findAll() {
        return ResponseEntity.ok().body(likeService.findAll());
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public List<Like> findByUserId(@PathVariable("id") Long user_id) {
        return likeService.findByUserId(user_id);
    }

    /**
     *
     * @param id : the id of a track
     * @param principal : the username (email) of user
     * @return Like data transfer object
     */

    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public LikeDTO addLikeToTrack(@PathVariable("id") Long id,
                                  @AuthenticationPrincipal Principal principal) {
        Like like = new Like();
        Like.LikePK primaryKey = new Like.LikePK();
        primaryKey.setUser(userService.findByEmail(principal.getName()));
        primaryKey.setTrack(trackService.findById(id));
        like.setLikePK(primaryKey);
        return new LikeDTO(likeService.addLikeToTrack(like));
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public Like deleteLike(@PathVariable("id") Long id) {
        return likeService.deleteLike(id);
    }
}
