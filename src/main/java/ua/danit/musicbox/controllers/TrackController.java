package ua.danit.musicbox.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.danit.musicbox.model.entity.Track;
import ua.danit.musicbox.model.entity.TrackDTO;
import ua.danit.musicbox.model.entity.User;
import ua.danit.musicbox.services.TrackService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/track")
public class TrackController {
    public final TrackService trackService;

    @Autowired
    public TrackController(TrackService trackService) {
        this.trackService = trackService;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Track> getTrackById(@PathVariable("id") Long id){
        Track track = trackService.findById(id);
        return ResponseEntity.ok().body(track);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<List<TrackDTO>> getAllTracks(){
        List<TrackDTO> trackDTOS;
        List<Track> tracks = trackService.findAll();
        trackDTOS = tracks.stream()
                .map(TrackDTO::new)
                .collect(Collectors.toList());
        return ResponseEntity.ok().body(trackDTOS);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Track> deleteTrack(@PathVariable("id")Long id){
        Track track = trackService.delete(id);
        return ResponseEntity.ok().body(track);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<Track> addTrack(@RequestBody Track track)  {
        return ResponseEntity.ok().body(trackService.add(track));
    }
}
