import React, {Component} from 'react'
import {Values} from "redux-form-website-template";
import ContactForm from '../components/forms/ContactForm'
import settings from "../constants/settings";

class Profile extends Component {

    handleSubmit = (values) => {
        const url=settings.api+"/login"
        fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: values
            })

        console.log(values);
    }



    render() {
        return (
            <div>
                <ContactForm onSubmit={this.handleSubmit}/>
                <Values form="contact"/>
            </div>
        );
    }
}

export default Profile