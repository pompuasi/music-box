import React, {Component} from 'react'
// import {connect} from 'react-redux'
import {Switch, Route, Link} from 'react-router-dom'
import Albums from '../components/Albums'
import settings from '../constants/settings'

class Default extends Component {
    constructor(props) {
        super(props)

        this.state = {
            error: null,
            isLoaded: false,
            choosenAlbum: null,
            albums: [],
            tracks: []
        }
    }

    componentDidMount() {
        const url = settings.api+"/album"
        fetch(url)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        albums: result
                    });
                },

                    (error) => {
                        this.setState({
                            isLoaded: true,
                            error
                        });
                    }
            )

    }

    render() {
        const {error, isLoaded, albums} = this.state
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <div className='container albums'>
                    <Albums albums={this.state.albums}/>
                </div>
            )
        }
    }
}

export default Default

