import React, {Component} from 'react'
import Track from "../components/Track";
import settings from "../constants/settings"

class Tracks extends Component {
    constructor(props) {
        super(props)

        this.state = {
            error: null,
            isLoaded: false,
            tracks: []
        }
    }



    componentDidMount() {

        const url = settings.api+settings.api_tracks
        fetch(url)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        tracks: result
                    });
                },

                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )

    }

    render(){
        return(
        <div className='container'>
            <Track tracks={this.state.tracks}/>
        </div>
        )
    }
}
// const mapStateToProps = (state, ownProps) => ({
//
//     album: state.find(album => album.id === +ownProps.match.params.albumId)
// });
// export default connect(mapStateToProps)(AlbumTracks)


export default Tracks