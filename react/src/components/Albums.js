import React from 'react'
import PropTypes from 'prop-types'
import settings from '../constants/settings'
import {Link} from 'react-router-dom'

class Albums extends React.Component {
    render() {
        return (
            <ul className='cover'>
                {this.props.albums.map(album => <li key={album.id}>
                    <Link to={settings.api_tracks + album.id}>
                    {/*<img src={settings.cover_path+album.cover} onClick={choosenAlbum=>album.id} alt={album.albumName}/> </Link>*/}
                    <img src={settings.cover_path+"/?"+album.id} onClick={choosenAlbum=>album.id} alt={album.albumName}/> </Link>
                </li>)}
            </ul>
        )
    }
}

Albums.propTypes = {
    albums: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        albumName: PropTypes.string.isRequired,
        cover: PropTypes.string.isRequired
        })
    ).isRequired
}

export default Albums

