import React from 'react'
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom'
import settings from "../constants/settings";

class Track extends React.Component{


    render() {
        return (
            <ul>
                {this.props.tracks.map(track => <li key={track.id}>{track.trackName+"      "}
                    <audio controls> <source src={settings.tracks_storage+track.link} type="audio/mpeg"/></audio>
                </li>)}
            </ul>
        )
    }
}

Track.propTypes = {
    tracks: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.number.isRequired,
            trackName: PropTypes.string.isRequired
        })
    ).isRequired
}

export default Track

