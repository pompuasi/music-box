import React from 'react'
import PropTypes from 'prop-types'

class TrackList extends React.Component {
    render(){
        return(
            <ul>
                {this.props.tracks.map(track => <li key={track.id}>
                    <Link className="track-name" to={'/'}>{track.trackName}</Link>
                </li>)}
            </ul>
        )
    }
}

export default TrackList