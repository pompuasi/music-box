import React, {Component} from 'react'
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom'

class Nav extends Component {
    render() {
        return (<nav>
            <header className="header">
                <div className="container">
                    <Link to='/'><img className="header__logo" src="logo.png" alt="Music Box"></img></Link>
                </div>
            </header>
            <div className="container">

                <ul>
                    {this.props.links.map(link => <li className="navigate" key={link.url}><Link className="navigate" to={link.url}>{link.text}</Link></li>)}
                </ul>
            </div>

        </nav>)
    }
}

Nav.propTypes = {
    title: PropTypes.string.isRequired,
    links: PropTypes.arrayOf(PropTypes.shape({
        url: PropTypes.string.isRequired,
        text: PropTypes.string.isRequired,
    })).isRequired
}

export default Nav