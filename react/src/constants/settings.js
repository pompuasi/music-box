const settings = {
    api: "http://127.0.0.1:8080/api",
    api_tracks: "/tracks/",
    cover_path: 'https://picsum.photos/200/200/',
    tracks_storage: 'http://s3.eu-west-3.amazonaws.com/koala-bucket/tracks/'
    // cover_path: 'http://s3.eu-west-3.amazonaws.com/koala-bucket/covers/',
    // tracks_storage: 'http://s3.eu-west-3.amazonaws.com/koala-bucket/tracks/'
}

export default settings