import React from 'react'
import {Switch, Route} from 'react-router-dom'

import Nav from './components/Nav'
import Default from './containers/Default'
import Favorite from './containers/Favorite'
import Profile from './containers/Profile'
import Admin from './containers/Admin'
import Tracks from './containers/Tracks'
import './styles.css'



class App extends React.Component {
    render() {
        const linksArr = [
            {url: '/', text: 'Music-box'},
            {url: '/tracks', text: 'Tracks'},
            {url: '/favorite', text: 'Favorite'},
            {url: '/profile', text: 'Profile'}
        ]
        return (
            <div>
                <Nav title='Music Box' links={linksArr} />
                <Switch>
                    <Route exact path='/' component={Default} />
                    <Route exact path='/tracks' component={Tracks} />
                    <Route path='/favorite' component={Favorite} />
                    <Route path='/profile' component={Profile} />
                    <Route path='/admin' component={Admin} />
                    <Route path='/' component={() => <h2>404</h2>} />
                </Switch>
            </div>
        )
    }
}

export default App