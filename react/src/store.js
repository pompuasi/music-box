import {createStore, combineReducers, applyMiddleware} from 'redux'
import {reducer as formReducer} from 'redux-form'
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk'
import counter from './reducers/index'
import load from './reducers/load'

const reducers = {
    counter,
    load,
    form: formReducer
}
const reducer = combineReducers(reducers)
const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)))

export default store
